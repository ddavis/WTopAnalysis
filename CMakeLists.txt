# Declare the name of this package:
atlas_subdir(WTopAnalysis)

# This package depends on other packages:
atlas_depends_on_subdirs(PUBLIC
  TopAnalysis
  TopConfiguration
  TopEvent
  TopEventSelectionTools
  TopObjectSelectionTools)

# This package uses ROOT:
find_package(ROOT)

# Custom definitions needed for this package:
# add_definitions(-g)

# Generate a CINT dictionary source file:
atlas_add_root_dictionary(WTopAnalysis WTopAnalysisCintDict
  ROOT_HEADERS
  WTopAnalysis/WTopAnalysisLoader.h
  WTopAnalysis/WTopEventSaver.h
  Root/LinkDef.h
  EXTERNAL_PACKAGES ROOT)

# Build a library that other components can link against:
atlas_add_library(WTopAnalysis
  WTopAnalysis/*.h Root/*.cxx ${WTopAnalysisCintDict}
  PUBLIC_HEADERS WTopAnalysis
  LINK_LIBRARIES TopAnalysis
  TopConfiguration
  TopEvent
  TopEventSelectionTools
  TopObjectSelectionTools
  ${ROOT_LIBRARIES}
  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS})

# Install user scripts and python modules
atlas_install_scripts(scripts/*)
atlas_install_python_modules(python/*.py)
