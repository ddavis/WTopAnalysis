#include "WTopAnalysis/WTopEventSaver.h"
#include "WTopAnalysis/WTopAnalysisLoader.h"
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class WTopEventSaver+;
#pragma link C++ class WTopAnalysisLoader+;

#endif
