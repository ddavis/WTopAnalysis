// WTopAnalysis
#include "WTopAnalysis/WTopEventSaver.h"

// ATLAS Top
#include "TopConfiguration/TopConfig.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"

// ATLAS Misc
#include "xAODTruth/xAODTruthHelpers.h"

// boost
#include <boost/algorithm/string.hpp>
#include <boost/range/adaptors.hpp>

// C++ STL
#include <fstream>

using boost::contains;
using boost::ends_with;
using boost::starts_with;
using boost::adaptors::indexed;

namespace {
/// remove branches from output n-tuple using pattern matching
int getBranchStatus(WTopEventSaver const* evtsaver, top::TreeManager const* tm,
                    std::string const& variableName) {
  if (starts_with(variableName, "weight_indiv_SF_")) return 0;
  if (starts_with(variableName, "weight_oldTriggerSF_")) return 0;
  if (starts_with(variableName, "jet_ip3d")) return 0;
  if (starts_with(variableName, "jet_jvt")) return 0;
  if (starts_with(variableName, "jet_DL1_")) return 0;
  if (starts_with(variableName, "jet_DL1r_")) return 0;
  if (starts_with(variableName, "jet_DL1rmu_")) return 0;
  if (starts_with(variableName, "jet_MV2")) return 0;
  if (ends_with(variableName, "_d0sig")) return 0;
  if (ends_with(variableName, "_delta_z0_sintheta")) return 0;
  if (ends_with(variableName, "_ptvarcone20")) return 0;
  if (ends_with(variableName, "_topoetcone20")) return 0;
  if (variableName == "jet_MV2c100") return 0;
  if (variableName == "jet_MV2c10mu") return 0;
  if (variableName == "jet_MV2c10rnn") return 0;
  if (variableName == "jet_MV2cl100") return 0;
  if (variableName == "jet_mv2c00") return 0;
  if (variableName == "jet_mv2c20") return 0;

  // disable loose systematic trees
  if (evtsaver->config()->doTightEvents()) {
    if (ends_with(tm->name(), "_Loose") && !contains(tm->name(), "nominal_")) return 0;

    // disable large weights branches for loose tree
    if (tm->name() == "nominal_Loose") {
      if (variableName == "mc_generator_weights") return 0;
      if (contains(variableName, "weight") &&
          (contains(variableName, "_UP") || contains(variableName, "_DOWN") ||
           contains(variableName, "_up") || contains(variableName, "_down")))
        return 0;
    }
  }
  // disable additional jet weights for systemtic and loose trees
  if (!(tm->name() == "nominal" ||
        (tm->name() == "nominal_Loose" && !(evtsaver->config()->doTightEvents())) ||
        tm->name() == "particleLevel" || tm->name() == "truth")) {
    if (starts_with(variableName, "jet_MV2")) return 0;
    if (starts_with(variableName, "jet_DL1_")) return 0;
    if (starts_with(variableName, "jet_DL1r_")) return 0;
    if (starts_with(variableName, "jet_DL1rmu_")) return 0;
  }
  // disable redundant JER trees
  if (starts_with(tm->name(), "JET_CategoryReduction_JET_JER_") &&
      (ends_with(tm->name(), "__1down") || ends_with(tm->name(), "__1down_Loose"))) {
    return 0;
  }

  // disable Isolation branches for all trees but nominal_Loose
  if (!(tm->name() == "nominal_Loose")) {
    if (starts_with(variableName, "el_Isol_") || starts_with(variableName, "mu_Isol_"))
      return 0;
  }

  return -1;
}
}  // namespace

// Always initialise primitive types in the constructor
WTopEventSaver::WTopEventSaver()
    : top::EventSaverFlatNtuple(),
      m_config(nullptr),
      m_lumiBlock(0),
      m_met_px(0.),
      m_met_py(0.),
      m_met_sumet(0.) {
  /* install the "getBranchStatus" hook */
  branchFilters().push_back(
      std::bind(&getBranchStatus, this, std::placeholders::_1, std::placeholders::_2));
}

void WTopEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file,
                                const std::vector<std::string>& extraBranches) {
  m_config = config;
  top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
  for (auto systematicTree : treeManagers()) {
    systematicTree->makeOutputVariable(m_lumiBlock, "lbn");
    systematicTree->makeOutputVariable(m_vtxz, "Vtxz");
    systematicTree->makeOutputVariable(m_npvtx, "npVtx");
    systematicTree->makeOutputVariable(m_met_px, "met_px");
    systematicTree->makeOutputVariable(m_met_py, "met_py");
    systematicTree->makeOutputVariable(m_met_sumet, "met_sumet");
    if (m_config->isMC() && m_config->doTopPartonHistory()) {
      systematicTree->makeOutputVariable(m_MC_t_beforeFSR_pt, "MC_t_beforeFSR_pt");
      systematicTree->makeOutputVariable(m_MC_tbar_beforeFSR_pt, "MC_tbar_beforeFSR_pt");
      systematicTree->makeOutputVariable(m_MC_t_afterFSR_pt, "MC_t_afterFSR_pt");
      systematicTree->makeOutputVariable(m_MC_tbar_afterFSR_pt, "MC_tbar_afterFSR_pt");
      systematicTree->makeOutputVariable(m_MC_ttbar_afterFSR_beforeDecay_m, "MC_ttbar_afterFSR_beforeDecay_m");
      systematicTree->makeOutputVariable(m_MC_ttbar_afterFSR_beforeDecay_pt, "MC_ttbar_afterFSR_beforeDecay_pt");
    }
  }

  /*
  //added outputs: truth neutrinos
  if (m_config->isMC() && m_config->doTopParticleLevel()) {
  //add particle level tree branches
  std::shared_ptr<top::TreeManager> plTreeManager = particleLevelTreeManager();
  plTreeManager->makeOutputVariable(m_nu_pt, "nu_pt");
  plTreeManager->makeOutputVariable(m_nu_eta, "nu_eta");
  plTreeManager->makeOutputVariable(m_nu_phi, "nu_phi");
  plTreeManager->makeOutputVariable(m_nu_origin, "nu_origin");
  }
  */
}

void WTopEventSaver::saveEvent(const top::Event& event) {
  //------- Filling up variables -----

  if (!(top::isSimulation(event))) m_lumiBlock = event.m_info->lumiBlock();
  m_npvtx = 0;
  m_vtxz = 0;
  const xAOD::VertexContainer* m_primvtx = event.m_primaryVertices;
  for (const auto* const vtxPtr : *m_primvtx) {
    const xAOD::VxType::VertexType vtype = vtxPtr->vertexType();
    const int vmult = vtxPtr->trackParticleLinks().size();
    // count vertices of type 1 (primary) and 3 (pileup) with >4 tracks
    if ((vtype == 1 || vtype == 3) && vmult > 4) {
      ++m_npvtx;
      // assuming there is only one primary vertex
      if (vtype == 1) m_vtxz = vtxPtr->z();
    }
  }

  // met
  m_met_px = event.m_met->mpx();
  m_met_py = event.m_met->mpy();
  m_met_sumet = event.m_met->sumet();

  // if parton history is requested, this saves the true top pt
  // information to the main tree
  if (m_config->isMC() && m_config->doTopPartonHistory()) {
    const xAOD::PartonHistoryContainer* partonHistoryContainer(nullptr);
    const xAOD::PartonHistory* partonHistory(nullptr);
    if (evtStore()->contains<xAOD::PartonHistoryContainer>(m_config->sgKeyTopPartonHistory())) {
      top::check(
          evtStore()->retrieve(partonHistoryContainer, m_config->sgKeyTopPartonHistory()),
          "Failed to retrieve top parton history");
      if (partonHistoryContainer->size() == 1) {
        partonHistory = partonHistoryContainer->at(0);
      }
    }
    static const SG::AuxElement::ConstAccessor<float> acc_MC_t_beforeFSR_pt{"MC_t_beforeFSR_pt"};
    static const SG::AuxElement::ConstAccessor<float> acc_MC_tbar_beforeFSR_pt{"MC_tbar_beforeFSR_pt"};
    static const SG::AuxElement::ConstAccessor<float> acc_MC_t_afterFSR_pt{"MC_t_afterFSR_pt"};
    static const SG::AuxElement::ConstAccessor<float> acc_MC_tbar_afterFSR_pt{"MC_tbar_afterFSR_pt"};
    static const SG::AuxElement::ConstAccessor<float> acc_MC_ttbar_afterFSR_beforeDecay_m{"MC_ttbar_afterFSR_beforeDecay_m"};
    static const SG::AuxElement::ConstAccessor<float> acc_MC_ttbar_afterFSR_beforeDecay_pt{"MC_ttbar_afterFSR_beforeDecay_pt"};

    if (partonHistory != nullptr) {
      m_MC_t_beforeFSR_pt = acc_MC_t_beforeFSR_pt(*partonHistory);
      m_MC_tbar_beforeFSR_pt = acc_MC_tbar_beforeFSR_pt(*partonHistory);
      m_MC_t_afterFSR_pt = acc_MC_t_afterFSR_pt(*partonHistory);
      m_MC_tbar_afterFSR_pt = acc_MC_tbar_afterFSR_pt(*partonHistory);
      m_MC_ttbar_afterFSR_beforeDecay_m = acc_MC_ttbar_afterFSR_beforeDecay_m(*partonHistory);
      m_MC_ttbar_afterFSR_beforeDecay_pt = acc_MC_ttbar_afterFSR_beforeDecay_pt(*partonHistory);
    }
    else {
      m_MC_t_beforeFSR_pt = -999;
      m_MC_tbar_beforeFSR_pt = -999;
      m_MC_t_afterFSR_pt = -999;
      m_MC_tbar_afterFSR_pt = -999;
      m_MC_ttbar_afterFSR_beforeDecay_m = -999;
      m_MC_ttbar_afterFSR_beforeDecay_pt = -999;
    }
  }

  top::EventSaverFlatNtuple::saveEvent(event);
}

/*
void WTopEventSaver::saveTruthEvent() {
  xAOD::EventInfo const* eventInfo = nullptr;
  xAOD::TruthEventContainer const* truthEvent = nullptr;
  top::check(evtStore()->retrieve(eventInfo, m_config->sgKeyEventInfo()),
             "Failed to retrieve EventInfo");
  top::check(evtStore()->retrieve(truthEvent, m_config->sgKeyTruthEvent()),
             "Failed to retrieve truth event container");
  top::check(truthEvent->size() == 1, "unexpected size of truth event container");
  top::EventSaverFlatNtuple::saveTruthEvent();
}
*/

/*
// add true neutrinos to particle-level events
//void WTopEventSaver::saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent) {
// Quick return if particle level is disabled. No tree will be created!
if (!m_config->doTopParticleLevel()) return;
// No need to attempt to write out anything for non-MC data.
if (!m_config->isMC()) return;

  const xAOD::TruthParticleContainer* neutrinos{nullptr};
  top::check(evtStore()->retrieve(neutrinos, "TruthNeutrinos"), "Failed to retrieve Truth
  Neutrinos"); m_nu_pt.resize(neutrinos->size()); m_nu_eta.resize(neutrinos->size());
  m_nu_phi.resize(neutrinos->size());
  m_nu_origin.resize(neutrinos->size());

  unsigned int i = 0;
  static const SG::AuxElement::ConstAccessor<unsigned int>
  acc_particleOrigin("particleOrigin"); static const SG::AuxElement::ConstAccessor<unsigned
  int> acc_classifierParticleOrigin("classifierParticleOrigin"); for (const auto& neutrino :
  *neutrinos) {

  unsigned int origin = 0;
  if (acc_particleOrigin.isAvailable(*neutrino)) {
  origin = acc_particleOrigin(*neutrino);
  }
  else if (acc_classifierParticleOrigin.isAvailable(*neutrino)) {
  origin = acc_classifierParticleOrigin(*neutrino);
  }
  else {
  std::cerr << "Could not obtain MCTruthClassifier result decoration"
  <<"(neutrino will be rejected by filter)." << std::endl;
  }
  if(origin !=10 && origin!=12 && origin!=13) continue;

  m_nu_pt[i] = neutrino->pt();
  m_nu_eta[i] = neutrino->eta();
  m_nu_phi[i] = neutrino->phi();
  m_nu_origin[i] = origin;
  ++i;
  }
*/

//  top::EventSaverFlatNtuple::saveParticleLevelEvent(plEvent);
//}

void WTopEventSaver::finalize() {
  top::EventSaverFlatNtuple::finalize();
  // std::ofstream f("WTopEventSaver-ok");
}
