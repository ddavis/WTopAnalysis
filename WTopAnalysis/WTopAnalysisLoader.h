#ifndef WTopAnalysis_WTopAnalysisLoader_h
#define WTopAnalysis_WTopAnalysisLoader_h

#include "TopEventSelectionTools/ToolLoaderBase.h"

class WTopAnalysisLoader : public top::ToolLoaderBase {
 public:
  top::EventSelectorBase* initTool(const std::string& name, const std::string& line,
                                   TFile* outputFile, std::shared_ptr<top::TopConfig> config,
                                   EL::Worker* wk = nullptr);

  ClassDef(WTopAnalysisLoader, 0)
};

#endif
