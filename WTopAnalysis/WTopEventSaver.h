#ifndef WTopAnalysis_WTopEventSaver_h
#define WTopAnalysis_WTopEventSaver_h

// ATLAS
#include "TopAnalysis/EventSaverFlatNtuple.h"

// C++ STL
#include <string>

class TH1;

class WTopEventSaver : public top::EventSaverFlatNtuple {
 public:
  /*!
   * @brief A default constructor so that this class can be created at
   * run-time by ROOT.
   */
  // Default - so root can load based on a name
  WTopEventSaver();

  /// default virtual destrutor
  virtual ~WTopEventSaver() = default;

  /*!
   * @brief initialise is run before the event loop.
   *
   * Run once at the start of the job
   *
   * @param config The configuration object (not used by the default flat
   * ntuple).
   * @param file The output file is used to put the TTrees in.
   * @param extraBranches Extra info requested (like which selections are
   * passed).
   *
   */
  virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file,
                          const std::vector<std::string>& extraBranches) override;

  // Keep the asg::AsgTool happy
  virtual StatusCode initialize() override { return StatusCode::SUCCESS; }

  /*!
   * @brief Run for every event (actually every systematic for every event).
   *
   * This does the tree filling work.
   * Run for every event (in every systematic) that needs saving
   *
   * @param event The top::Event which has had object selection and overlap
   * removal (if requested) applied to it.
   */
  virtual void saveEvent(const top::Event& event) override;

  // void saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent) override;
  // void saveTruthEvent() override;

  /*!
   * @brief Not used by the flat ntuple code yet, but needed by the xAOD code.
   */
  virtual void finalize() override;

  const top::TopConfig* config() const { return m_config.get(); }

 private:
  /// Configuration
  std::shared_ptr<top::TopConfig> m_config;

  // event info
  unsigned int m_lumiBlock;
  unsigned int m_npvtx;
  float m_vtxz;

  // met variables
  float m_met_px;
  float m_met_py;
  float m_met_sumet;

  float m_MC_t_beforeFSR_pt;
  float m_MC_tbar_beforeFSR_pt;
  float m_MC_t_afterFSR_pt;
  float m_MC_tbar_afterFSR_pt;
  float m_MC_ttbar_afterFSR_beforeDecay_m;
  float m_MC_ttbar_afterFSR_beforeDecay_pt;

  ClassDefOverride(WTopEventSaver, 0);
};

#endif
