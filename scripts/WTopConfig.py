#!/usr/bin/env python

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

import argparse


def get_args():
    """get the parsed arguments namespace

    Returns
    -------
    argparse.Namespace
        the command line arguments namespace
    """
    parser = argparse.ArgumentParser(description="Generate config file")
    parser.add_argument("outfile", type=str, help="output file name")
    parser.add_argument("--campaign", type=str, required=True, help="MC campaign")
    parser.add_argument("--systematics", type=str, required=True, help="Systematic setup")
    parser.add_argument("--truth-on", action="store_true", help="turn on truth information")
    parser.add_argument("--gen-weights", action="store_true", help="turn on MC generator weights")
    parser.add_argument("--nevents", type=int, required=False, help="total number of events")
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()

    out = open(args.outfile, "w")

    def put(**kwargs):
        for k, v in kwargs.iteritems():
            out.write("%s %s\n" % (k, v))

    def br():
        out.write("\n")

    put(LibraryNames="libTopEventSelectionTools libTopEventReconstructionTools libWTopAnalysis")
    put(OutputFileSetAutoFlushZero="True")
    br()

    put(GRLDir="GoodRunsLists")
    if args.campaign == "mc16a" or args.campaign == "data15" or args.campaign == "data16":
        put(GRLFile="data15_13TeV/20170619/physics_25ns_21.0.19.xml data16_13TeV/20180129/physics_25ns_21.0.19.xml")
    elif args.campaign == "mc16c" or args.campaign == "mc16d" or args.campaign == "data17":
        put(GRLFile="data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml")
    elif args.campaign == "mc16e" or args.campaign == "data18":
        put(GRLFile="data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.xml")
    br()

    if args.campaign == "mc16a":
        prwfilename_FS = "dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/prw.merged.root"
        prwfilename_AF = "dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.AF.v2/prw.merged.root"
        prwconfigfile = ("GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root "
                       "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root")
        #print "Using (FS): " + prwfilename_FS
        #print "Using (AFII): " + prwfilename_AF
        put(PRWConfigFiles_FS="%s" % prwfilename_FS)
        put(PRWConfigFiles_AF="%s" % prwfilename_AF)
        put(PRWLumiCalcFiles="%s" % prwconfigfile)
    elif args.campaign == "mc16d":
        prwfilename_FS = "dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.FS.v2/prw.merged.root"
        prwfilename_AF = "dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.AF.v2/prw.merged.root"
        prwActualMu = "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
        prwconfigfile = "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
        #print "Using (FS): " + prwfilename_FS
        #print "Using (AFII): " + prwfilename_AF
        put(PRWConfigFiles_FS="%s" % prwfilename_FS)
        put(PRWConfigFiles_AF="%s" % prwfilename_AF)
        put(PRWActualMu_FS="%s" % prwActualMu)
        put(PRWActualMu_AF="%s" % prwActualMu)
        put(PRWLumiCalcFiles="%s" % prwconfigfile)
    elif args.campaign == "mc16e":
        prwfilename_FS = "dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16e.FS.v2/prw.merged.root"
        prwfilename_AF = "dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16e.AF.v2/prw.merged.root"
        prwActualMu = "GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"
        prwconfigfile = "GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"
        #print "Using (FS): " + prwfilename_FS
        #print "Using (AFII): " + prwfilename_AF
        put(PRWConfigFiles_FS="%s" % prwfilename_FS)
        put(PRWConfigFiles_AF="%s" % prwfilename_AF)
        put(PRWActualMu_FS="%s" % prwActualMu)
        put(PRWActualMu_AF="%s" % prwActualMu)
        put(PRWLumiCalcFiles="%s" % prwconfigfile)
    br()

    put(BTagCDIPath="xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v3.root")

    put(UseGlobalLeptonTriggerSF="True")
    put(GlobalTriggers="2015@2e12_lhloose_L12EM10VH,2mu10,mu18_mu8noL1,e17_lhloose_mu14,e7_lhmedium_mu24,e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50 2016@2e17_lhvloose_nod0,2mu14,mu22_mu8noL1,e17_lhloose_nod0_mu14,e7_lhmedium_nod0_mu24,e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2017@2e24_lhvloose_nod0,2mu14,mu22_mu8noL1,e17_lhloose_nod0_mu14,e7_lhmedium_nod0_mu24,e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@2e24_lhvloose_nod0,2mu14,mu22_mu8noL1,e17_lhloose_nod0_mu14,e7_lhmedium_nod0_mu24,e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50")
    put(GlobalTriggersLoose="2015@2e12_lhloose_L12EM10VH,2mu10,mu18_mu8noL1,e17_lhloose_mu14,e7_lhmedium_mu24,e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50 2016@2e17_lhvloose_nod0,2mu14,mu22_mu8noL1,e17_lhloose_nod0_mu14,e7_lhmedium_nod0_mu24,e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2017@2e24_lhvloose_nod0,2mu14,mu22_mu8noL1,e17_lhloose_nod0_mu14,e7_lhmedium_nod0_mu24,e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@2e24_lhvloose_nod0,2mu14,mu22_mu8noL1,e17_lhloose_nod0_mu14,e7_lhmedium_nod0_mu24,e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50")
    br()


    ### Object container names
    put(ElectronCollectionName="Electrons")
    put(MuonCollectionName="Muons")
    put(JetCollectionName="AntiKt4EMPFlowJets_BTagging201903")
    put(LargeJetCollectionName="None")
    put(TauCollectionName="None")
    put(PhotonCollectionName="None")
    put(TrackJetCollectionName="None")
    put(JetGhostTrackDecoName="None")
    br()

    ### Large-R configuration
    put(LargeRJESJMSConfig="CombMass")
    put(LargeRJetPt="200000")
    put(LargeRJetEta="2")
    put(LargeJetSubstructure="None")
    br()

    ### Reclustered jet configuration
    put(UseRCJets="False")
    br()

    ### Electron configuration
    put(ElectronPt="20000")
    put(ElectronID="TightLH")
    put(ElectronIDLoose="LooseAndBLayerLH")
    put(ElectronIsolation="Gradient")
    put(ElectronIsolationLoose="None")
    br()

    ### Muon configuration
    put(MuonPt="20000")
    put(MuonQuality="Medium")
    put(MuonQualityLoose="Medium")
    put(MuonIsolation="FCTight_FixedRad")
    put(MuonIsolationLoose="None")
    br()

    ### Jet configuration
    put(JetPt="20000")
    br()

    ### Object loader/ output configuration
    put(ObjectSelectionName="top::ObjectLoaderStandardCuts")
    put(OutputFormat="WTopEventSaver")
    put(OutputEvents="SelectedEvents")
    put(OutputFilename="output.root")
    put(PerfStats="No")
    br()

    ### B-tagging configuration
    put(BTaggingWP="DL1r:FixedCutBEff_77 DL1r:FixedCutBEff_70 DL1r:FixedCutBEff_60 DL1r:Continuous")
    br()

    # DoTight/DoLoose to activate the loose and tight trees
    # each should be one in: Data, MC, Both, False
    put(DoTight="Both")
    put(DoLoose="False")
    br()

    # Turn on MetaData to pull IsAFII from metadata
    put(UseAodMetaData="True")
    br()

    ### Truth configuration
    put(TruthCollectionName="TruthParticles")
    put(TruthJetCollectionName="AntiKt4TruthDressedWZJets")
    put(TruthLargeRJetCollectionName="None")
    put(TopPartonHistory="ttbar")
    put(TopParticleLevel="True" if args.truth_on else "False")
    put(TruthBlockInfo="False")
    #put(PDFInfo="True" if args.truth_on else "False")
    br()

    ### Systematics configuration
    put(Systematics=args.systematics)
    put(JetUncertainties_NPModel="CategoryReduction")
    put(JetUncertainties_BunchSpacing="25ns")
    br()

    ### Extra weights
    if args.gen_weights:
        put(MCGeneratorWeights="Nominal")
        br()

    ### Total number of events
    if args.nevents is not None:
        put(NEvents="{}".format(args.nevents))
        br()

    br()
    out.write("""
########################
### basic selection with mandatory cuts for reco level
########################

SUB BASIC
INITIAL
GRL
GOODCALO
PRIVTX
RECO_LEVEL

########################
### definition of the data periods
########################

SUB period_2015
RUN_NUMBER >= 276262
RUN_NUMBER <= 284484

SUB period_2016
RUN_NUMBER >= 296939
RUN_NUMBER <= 311481

SUB period_2017
RUN_NUMBER >= 324320
RUN_NUMBER <= 341649

SUB period_2018
RUN_NUMBER >= 348197

########################
### emu selections
########################

SUB EM_2015
. BASIC
. period_2015
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB EM_2016
. BASIC
. period_2016
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB EM_2017
. BASIC
. period_2017
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB EM_2018
. BASIC
. period_2018
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB emu_basic
JET_N 25000 <= 2
JET_N 25000 >= 1
EL_N 20000 >= 1
MU_N 20000 >= 1
GTRIGMATCH
JETCLEAN LooseBad
NOBADMUON

SELECTION emu_2015
. EM_2015
. emu_basic
SAVE

SELECTION emu_2016
. EM_2016
. emu_basic
SAVE

SELECTION emu_2017
. EM_2017
. emu_basic
SAVE

SELECTION emu_2018
. EM_2018
. emu_basic
SAVE

SELECTION emu_particle
PRIVTX
PARTICLE_LEVEL
EL_N_OR_MU_N 27000 >= 1
. emu_basic
SAVE
""")

########################
### ee selections
########################

# SUB EL_2015
# . BASIC
# . period_2015
# GTRIGDEC
# EL_N 27000 >= 1

# SUB EL_2016
# . BASIC
# . period_2016
# GTRIGDEC
# EL_N 27000 >= 1

# SUB EL_2017
# . BASIC
# . period_2017
# GTRIGDEC
# EL_N 27000 >= 1

# SUB EL_2018
# . BASIC
# . period_2018
# GTRIGDEC
# EL_N 27000 >= 1

# SUB ee_basic
# EL_N 27000 >= 1
# GTRIGMATCH
# JETCLEAN LooseBad
# EL_N 25000 == 2
# MU_N 25000 == 0
# OS
# NOBADMUON

# SELECTION ee_2015
# . EL_2015
# . ee_basic
# SAVE

# SELECTION ee_2016
# . EL_2016
# . ee_basic
# SAVE

# SELECTION ee_2017
# . EL_2017
# . ee_basic
# SAVE

# SELECTION ee_2018
# . EL_2018
# . ee_basic
# SAVE

# SELECTION ee_particle
# PRIVTX
# PARTICLE_LEVEL
# EL_N 27000 >= 1
# . ee_basic
# SAVE

########################
### mumu selections
########################

# SUB MU_2015
# . BASIC
# . period_2015
# GTRIGDEC
# MU_N 25000 >= 1

# SUB MU_2016
# . BASIC
# . period_2016
# GTRIGDEC
# MU_N 27000 >= 1

# SUB MU_2017
# . BASIC
# . period_2017
# GTRIGDEC
# MU_N 27000 >= 1

# SUB MU_2018
# . BASIC
# . period_2018
# GTRIGDEC
# MU_N 27000 >= 1

# SUB mumu_basic
# MU_N 27000 >= 1
# GTRIGMATCH
# #EMU_OVERLAP
# JETCLEAN LooseBad
# JET_N 25000 >= 1
# MU_N 25000 == 2
# EL_N 25000 == 0
# OS
# NOBADMUON

# SELECTION mumu_2015
# . MU_2015
# . mumu_basic
# SAVE

# SELECTION mumu_2016
# . MU_2016
# . mumu_basic
# SAVE

# SELECTION mumu_2017
# . MU_2017
# . mumu_basic
# SAVE

# SELECTION mumu_2018
# . MU_2018
# . mumu_basic
# SAVE

# SELECTION mumu_particle
# PRIVTX
# PARTICLE_LEVEL
# MU_N 27000 >= 1
# . mumu_basic
# SAVE


    out.close()
