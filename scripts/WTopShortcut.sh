#!/bin/bash


GEN=""
SUB=""
SUF=""

function usage() {
    echo "Flags and options:"
    echo "  -g, --gen           Generate configs."
    echo "  -s, --sub           Submit jobs."
    echo "  -x, --suffix [STR]  Output DS name suffix."
    echo "  -h, --help          Print help message and exit."
    echo ""
}

while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        -h|--help)
            usage
            exit 0
            ;;
        -g|--gen)
            GEN="1"
            ;;
        -s|--sub)
            SUB="1"
            ;;
        -x|-suffix)
            shift
            SUF="$1"
            ;;
        *)
            echo "unknown option '$key'"
            usage
            exit 1
            ;;
    esac
    shift
done


if [ "$GEN" = "1" ]; then
    WTopConfig.py --campaign mc16a --systematics All --gen-weights --truth-on config.full.mc16a.txt
    WTopConfig.py --campaign mc16d --systematics All --gen-weights --truth-on config.full.mc16d.txt
    WTopConfig.py --campaign mc16e --systematics All --gen-weights --truth-on config.full.mc16e.txt

    WTopConfig.py --campaign mc16a --systematics Nominal config.minimal.mc16a.txt
    WTopConfig.py --campaign mc16d --systematics Nominal config.minimal.mc16d.txt
    WTopConfig.py --campaign mc16e --systematics Nominal config.minimal.mc16e.txt

    WTopConfig.py --campaign data15 --systematics Nominal config.data15.txt
    WTopConfig.py --campaign data16 --systematics Nominal config.data16.txt
    WTopConfig.py --campaign data17 --systematics Nominal config.data17.txt
    WTopConfig.py --campaign data18 --systematics Nominal config.data18.txt
fi

if [ "$SUB" = "1" ]; then
    [[ -z "$SUF" ]] && echo "A suffix required (via -x or --suffix)" && exit 1

    WTopSubmitToGrid.py --config config.data15.txt --suffix "$SUF" --to-bnl Data15
    WTopSubmitToGrid.py --config config.data16.txt --suffix "$SUF" --to-bnl Data16
    WTopSubmitToGrid.py --config config.data17.txt --suffix "$SUF" --to-bnl Data17
    WTopSubmitToGrid.py --config config.data18.txt --suffix "$SUF" --to-bnl Data18

    WTopSubmitToGrid.py --config config.full.mc16a.txt --suffix "$SUF" --to-bnl mc16a_13TeV_tW mc16a_13TeV_ttbar
    WTopSubmitToGrid.py --config config.full.mc16d.txt --suffix "$SUF" --to-bnl mc16d_13TeV_tW mc16d_13TeV_ttbar
    WTopSubmitToGrid.py --config config.full.mc16e.txt --suffix "$SUF" --to-bnl mc16e_13TeV_tW mc16e_13TeV_ttbar

    WTopSubmitToGrid.py --config config.minimal.mc16a.txt --suffix "$SUF" --to-bnl \
                        mc16a_13TeV_ttbar_Syst \
                        mc16a_13TeV_tW_Syst \
                        mc16a_13TeV_Wtaunu_Sherpa221 \
                        mc16a_13TeV_Wenu_Sherpa221 \
                        mc16a_13TeV_Wmunu_Sherpa221 \
                        mc16a_13TeV_Zmumu_Sherpa221 \
                        mc16a_13TeV_Zee_Sherpa221 \
                        mc16a_13TeV_Ztautau_Sherpa221 \
                        mc16a_13TeV_Diboson_Sherpa222

    WTopSubmitToGrid.py --config config.minimal.mc16d.txt --suffix "$SUF" --to-bnl \
                        mc16d_13TeV_ttbar_Syst \
                        mc16d_13TeV_tW_Syst \
                        mc16d_13TeV_Wtaunu_Sherpa221 \
                        mc16d_13TeV_Wenu_Sherpa221 \
                        mc16d_13TeV_Wmunu_Sherpa221 \
                        mc16d_13TeV_Zmumu_Sherpa221 \
                        mc16d_13TeV_Zee_Sherpa221 \
                        mc16d_13TeV_Ztautau_Sherpa221 \
                        mc16d_13TeV_Diboson_Sherpa222

    WTopSubmitToGrid.py --config config.minimal.mc16e.txt --suffix "$SUF" --to-bnl \
                        mc16e_13TeV_ttbar_Syst \
                        mc16e_13TeV_tW_Syst \
                        mc16e_13TeV_Wtaunu_Sherpa221 \
                        mc16e_13TeV_Wenu_Sherpa221 \
                        mc16e_13TeV_Wmunu_Sherpa221 \
                        mc16e_13TeV_Zmumu_Sherpa221 \
                        mc16e_13TeV_Zee_Sherpa221 \
                        mc16e_13TeV_Ztautau_Sherpa221 \
                        mc16e_13TeV_Diboson_Sherpa222
fi
