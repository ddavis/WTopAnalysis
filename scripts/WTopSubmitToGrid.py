#!/usr/bin/env python

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

import TopExamples.grid
import WTopAnalysis.WTopMCSamples

import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Submit to grid script")
    parser.add_argument("samples", type=str, nargs="+")
    parser.add_argument("--config", type=str, help="config file", required=True)
    parser.add_argument("--suffix", type=str, required=True, help="grid sample suffix")
    parser.add_argument("--no-submit", action="store_true", help="no submit flag")
    parser.add_argument("--to-bnl", action="store_true", help="store at BNL")
    args = parser.parse_args()

    config = TopExamples.grid.Config()
    config.gridUsername  = "ddavis"
    config.code          = "top-xaod"
    config.settingsFile  = args.config
    config.suffix        = args.suffix
    config.noSubmit      = args.no_submit
    config.excludedSites = ""
    config.CMake         = True
    config.mergeType     = "Default" # "None", "Default" or "xAOD"
    config.destSE        = "BNL-OSG2_LOCALGROUPDISK" if args.to_bnl else ""

    samples = args.samples
    samples = TopExamples.grid.Samples(samples)
    TopExamples.grid.submit(config, samples)
