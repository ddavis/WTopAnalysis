## -*- mode: conf -*-

LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libWTopAnalysis

### Good Run List
GRLDir  GoodRunsLists
GRLFile data15_13TeV/20170619/physics_25ns_21.0.19.xml data16_13TeV/20180129/physics_25ns_21.0.19.xml data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml

### Pile-up reweighting tool - Metadata determines which to use
# MC16a configuration

### This setup is for the CI tests
#PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/CI.prw.merged.root
#PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.AF.v2/CI.prw.merged.root

### Use following lines (uncomment) for your config file
#PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.FS.v2/prw.merged.root
#PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16a.AF.v2/prw.merged.root

#PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root

# MC16d configuration
PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.FS.v2/CI.prw.merged.root
PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.AF.v2/CI.prw.merged.root
PRWActualMu_FS GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRWActualMu_AF GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRWLumiCalcFiles GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root

# MC16e configuration
#PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16e.FS.v2/prw.merged.root
#PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16e.AF.v2/prw.merged.root
#PRWActualMu_FS GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
#PRWActualMu_AF GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
#PRWLumiCalcFiles GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root

### Object container names
ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMPFlowJets_BTagging201903
LargeJetCollectionName None
TauCollectionName None
PhotonCollectionName None
TrackJetCollectionName None
JetGhostTrackDecoName None

### Large-R configuration
LargeRJESJMSConfig CombMass
LargeRJetPt 200000
LargeRJetEta 2
LargeJetSubstructure None

### Reclustered jet configuration
UseRCJets False

### Truth configuration
TruthCollectionName TruthParticles
TruthJetCollectionName AntiKt4TruthDressedWZJets
TruthLargeRJetCollectionName None
TopPartonHistory ttbar
TopParticleLevel True
TruthBlockInfo False
PDFInfo True

### Object loader/ output configuration
ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat WTopEventSaver
OutputEvents SelectedEvents
OutputFilename output.root
PerfStats No

### Systematics configuration
Systematics All
JetUncertainties_NPModel CategoryReduction
JetUncertainties_BunchSpacing 25ns

### Electron configuration
ElectronPt 20000
ElectronID TightLH
ElectronIDLoose LooseAndBLayerLH
ElectronIsolation Gradient
ElectronIsolationLoose None

### Muon configuration
MuonPt 20000
MuonQuality Medium
MuonQualityLoose Medium
MuonIsolation FCTight_FixedRad
MuonIsolationLoose None

### Jet configuration
JetPt 20000

### Extra weights
MCGeneratorWeights Nominal

### Total number of events
NEvents 1000

# DoTight/DoLoose to activate the loose and tight trees
# each should be one in: Data, MC, Both, False
DoTight Both
DoLoose False

# Turn on MetaData to pull IsAFII from metadata
UseAodMetaData True
#IsAFII False

### B-tagging configuration
BTaggingWP DL1r:FixedCutBEff_77 DL1r:FixedCutBEff_70 DL1r:FixedCutBEff_60 DL1r:Continuous

### Global lepton trigger scale factor example
UseGlobalLeptonTriggerSF True
GlobalTriggers 2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50 2016@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50
GlobalTriggersLoose 2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose,mu20_iloose_L1MU15_OR_mu50 2016@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50 2018@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,mu26_ivarmedium_OR_mu50

########################
### basic selection with mandatory cuts for reco level
########################

SUB BASIC
INITIAL
GRL
GOODCALO
PRIVTX
RECO_LEVEL

########################
### definition of the data periods
########################

SUB period_2015
RUN_NUMBER >= 276262
RUN_NUMBER <= 284484

SUB period_2016
RUN_NUMBER >= 296939
RUN_NUMBER <= 311481

SUB period_2017
RUN_NUMBER >= 324320
RUN_NUMBER <= 341649

SUB period_2018
RUN_NUMBER >= 348197

########################
### emu selections
########################

SUB EM_2015
. BASIC
. period_2015
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB EM_2016
. BASIC
. period_2016
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB EM_2017
. BASIC
. period_2017
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB EM_2018
. BASIC
. period_2018
GTRIGDEC
EL_N_OR_MU_N 27000 >= 1

SUB emu_basic
JET_N 25000 <= 2
JET_N 25000 >= 1
EL_N 20000 >= 1
MU_N 20000 >= 1
GTRIGMATCH
JETCLEAN LooseBad
NOBADMUON

SELECTION emu_2015
. EM_2015
. emu_basic
SAVE

SELECTION emu_2016
. EM_2016
. emu_basic
SAVE

SELECTION emu_2017
. EM_2017
. emu_basic
SAVE

SELECTION emu_2018
. EM_2018
. emu_basic
SAVE

SELECTION emu_particle
PRIVTX
PARTICLE_LEVEL
EL_N_OR_MU_N 27000 >= 1
. emu_basic
SAVE

########################
### ee selections
########################

# SUB EL_2015
# . BASIC
# . period_2015
# GTRIGDEC
# EL_N 27000 >= 1

# SUB EL_2016
# . BASIC
# . period_2016
# GTRIGDEC
# EL_N 27000 >= 1

# SUB EL_2017
# . BASIC
# . period_2017
# GTRIGDEC
# EL_N 27000 >= 1

# SUB EL_2018
# . BASIC
# . period_2018
# GTRIGDEC
# EL_N 27000 >= 1

# SUB ee_basic
# EL_N 27000 >= 1
# GTRIGMATCH
# JETCLEAN LooseBad
# EL_N 25000 == 2
# MU_N 25000 == 0
# OS
# NOBADMUON

# SELECTION ee_2015
# . EL_2015
# . ee_basic
# SAVE

# SELECTION ee_2016
# . EL_2016
# . ee_basic
# SAVE

# SELECTION ee_2017
# . EL_2017
# . ee_basic
# SAVE

# SELECTION ee_2018
# . EL_2018
# . ee_basic
# SAVE

# SELECTION ee_particle
# PRIVTX
# PARTICLE_LEVEL
# EL_N 27000 >= 1
# . ee_basic
# SAVE

########################
### mumu selections
########################

# SUB MU_2015
# . BASIC
# . period_2015
# GTRIGDEC
# MU_N 25000 >= 1

# SUB MU_2016
# . BASIC
# . period_2016
# GTRIGDEC
# MU_N 27000 >= 1

# SUB MU_2017
# . BASIC
# . period_2017
# GTRIGDEC
# MU_N 27000 >= 1

# SUB MU_2018
# . BASIC
# . period_2018
# GTRIGDEC
# MU_N 27000 >= 1

# SUB mumu_basic
# MU_N 27000 >= 1
# GTRIGMATCH
# #EMU_OVERLAP
# JETCLEAN LooseBad
# JET_N 25000 >= 1
# MU_N 25000 == 2
# EL_N 25000 == 0
# OS
# NOBADMUON

# SELECTION mumu_2015
# . MU_2015
# . mumu_basic
# SAVE

# SELECTION mumu_2016
# . MU_2016
# . mumu_basic
# SAVE

# SELECTION mumu_2017
# . MU_2017
# . mumu_basic
# SAVE

# SELECTION mumu_2018
# . MU_2018
# . mumu_basic
# SAVE

# SELECTION mumu_particle
# PRIVTX
# PARTICLE_LEVEL
# MU_N 27000 >= 1
# . mumu_basic
# SAVE
